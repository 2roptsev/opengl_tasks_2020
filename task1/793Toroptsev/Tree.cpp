#include <string>
#include <stack>
#include "Tree.hpp"
#include "Branch.hpp"

Tree::Tree(const std::string &lString) {
    std::stack<std::pair<Branch, float>> stack;

    Branch current(0.1, 0.102, 0.04, 20);
    float low_r = 0.101f;

    for (int i = 0; i < lString.length(); i++) {
        char symbol = lString[i];
        std::cout << symbol;
        if (symbol == 'X') {
            continue;
        }

        if (symbol == 'F') {
            Branch b = current;
            if (b.get_bottom_r() > 0.001) {
                branches.push_back(b);
            }
            current.move(b.get_shift());
            current.shape(low_r/current.get_bottom_r(), 1);
            low_r *= 300.0 / 310.0;
        } else if (symbol == '-') {
            current.rotate(glm::vec3(1, 0, 0), PI / 5);
        } else if (symbol == '+') {
            current.rotate(glm::vec3(1, 0, 0), -PI / 8);
        } else if (symbol == '<') {
            current.rotate(glm::vec3(0, 0, 1), PI / 4);
        } else if (symbol == '>') {
            current.rotate(glm::vec3(0, 0, 1), -PI / 4);
        } else if (symbol == '[') {
            stack.push({current, low_r});
        } else if (symbol == ']') {
            auto p = stack.top();
            current = p.first;
            low_r = p.second;
            stack.pop();
        }
    }
}

int Tree::size() const {
    int result = 0;
    for (auto branch : branches) {
        result += branch.size();
    }
    return result;
}

std::vector<float> Tree::to_points() const {
    int size = this->size();
    std::vector<float> result;
    result.reserve(size);
    for (auto branch : branches) {
        std::vector<float> branch_points = branch.to_points();
        result.insert(result.end(), branch_points.begin(), branch_points.end());
    }
    return result;
}

std::vector<float> Tree::to_normals() const {
    int size = this->size();
    std::vector<float> result;
    result.reserve(size);
    for (auto branch : branches) {
        std::vector<float> branch_normals = branch.to_normals();
        result.insert(result.end(), branch_normals.begin(), branch_normals.end());
    }
    return result;
}