#include "TreeApplication.hpp"
#include "LSystem.hpp"
#include "Tree.hpp"

void TreeApplication::makeScene() {
    Application::makeScene();
    LSystem lSystem("X", {{'X', "F-[[<<X>]+X>]>+F[+F<X>]-X"}, {'F', "FF"}});
    Branch branch(1, 1, 1, 10);
    Branch branch2 = branch;
    branch.move(1);
    branch.rotate(glm::vec3(1, 0, 0), PI / 4);
    branch.shape(1, 2);


    auto lString = lSystem.evolve(6);
    //std::cout << lString << std::endl;
    Tree tree(lString);

    std::vector<float> points = tree.to_points();

    DataBufferPtr points_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);

    points_buf->setData(points.size() * sizeof(float), points.data());

    tree_mesh_ = std::make_shared<Mesh>();

    tree_mesh_->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0, points_buf);

    tree_mesh_->setPrimitiveType(GL_TRIANGLES);

    tree_mesh_->setVertexCount(points.size() / 3);

    std::vector<float> normals = tree.to_normals();

    DataBufferPtr normals_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);

    normals_buf->setData(normals.size() * sizeof(float), normals.data());

    tree_mesh_->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0, normals_buf);

    glm::tmat4x4<float> m = glm::mat4(1.0);

    tree_mesh_->setModelMatrix(m);

    _cameraMover = std::make_shared<FreeCameraMover>();

    shader_ = std::make_shared<ShaderProgram>("793ToroptsevData1/shader.vert", "793ToroptsevData1/shader.frag");
}

void TreeApplication::draw() {
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader_->use();
    shader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    shader_->setMat4Uniform("modelMatrix", tree_mesh_->modelMatrix());

    tree_mesh_->draw();
}
