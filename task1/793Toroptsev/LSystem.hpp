#pragma once

class LSystem {
public:
    LSystem(std::string initial, std::map<char, std::string> rules):
            initial(std::move(initial)),
            rules(std::move(rules))
    {
        state = this->initial;
    };

    std::string evolve(int n_iterations) {
        for (int i = 0; i < n_iterations; i++) {
            evolve_step();
        }
        return state;
    }

private:
    std::string initial;
    std::map<char, std::string> rules;
    std::string state;

    void evolve_step();
};
