#pragma once

#include <glm/vec3.hpp>
#include <cmath>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>
#include <vector>

const float PI = glm::pi<float>();

class Branch {
//traingulated frustum, defined with bottom radius, top radius and height
public:
    Branch (float top_radius, float bot_radius, float height, int fragmentation);

    void move(glm::vec3 shift);
    void move(float dist);
    void shape(float r, float h);
    void rotate(glm::vec3 ax, float val);

    glm::vec3 get_shift() const;
    float get_bottom_r() const;

    std::vector<float> to_normals() const;
    std::vector<float> to_points() const;

    int size() const;

private:
    float top_radius;
    float bot_radius;
    float height;
    int fragmentation;

    glm::vec3 center;
    glm::vec3 axis;

    float l;

    std::vector<glm::vec3> points;
    std::vector<glm::vec3> normals;

    glm::vec3 next(const glm::vec3 &input) const;

    void build();
};
