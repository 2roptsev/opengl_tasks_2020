#pragma once

#include "Branch.hpp"

class Tree {
public:
    explicit Tree (std::vector<Branch> branches): branches(std::move(branches)) {};

    explicit Tree(const std::string &lString);

    int size() const;

    std::vector<float> to_points() const;

    std::vector<float> to_normals() const;

private:
    std::vector<Branch> branches;
};