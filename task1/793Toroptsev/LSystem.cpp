#include <string>
#include <map>
#include "LSystem.hpp"

void LSystem::evolve_step() {
    std::string buffer;
    std::map<char, std::string>::const_iterator it;

    for (auto symbol : state) {
        it = rules.find(symbol);

        if (it != rules.end()) {
            buffer += it->second;
        } else {
            buffer += symbol;
        }
    }

    state = buffer;
}

