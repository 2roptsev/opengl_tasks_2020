#pragma once

#include "common/Application.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Mesh.hpp"

class TreeApplication : public Application
{
    ShaderProgramPtr shader_;
    MeshPtr tree_mesh_;

public:
    //Идентификатор шейдерной программы
    GLuint _program;

    void makeScene() override;

    void draw() override;
};