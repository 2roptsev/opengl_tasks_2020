#include "Branch.hpp"

Branch::Branch(float top_radius, float bot_radius, float height, int fragmentation):
        fragmentation(fragmentation)
{
    this->top_radius = top_radius;
    this->bot_radius = bot_radius;
    this->height = height;

    center = glm::vec3(0, 0, 0);
    axis = glm::vec3(0, 0, 1);

    l = sqrt(height * height + (bot_radius - top_radius) * (bot_radius - top_radius));
    build();
}

void Branch::move(glm::vec3 shift) {
    for (int i = 0; i < points.size(); i++) {
        points[i] += shift;
    }
    center += shift;
}

void Branch::move(float dist) {
    for (int i = 0; i < points.size(); i++) {
        points[i] += axis * dist;
    }
    center += axis * dist;
}

void Branch::shape(float r, float h) {
    Branch new_branch(top_radius * r, bot_radius * r, height * h, fragmentation);
    new_branch.move(center);
    //*this = new_branch;

    auto rot_axis = glm::cross(axis, new_branch.axis);

    if (length(rot_axis) == 0) {
        *this = new_branch;
        return;
    }

    std::cout << std::endl;
    std::cout << rot_axis.x << ' ' << rot_axis.y << ' ' << rot_axis.z <<  ' ' << std::endl;

    rot_axis /= length(rot_axis);
    auto angle = glm::orientedAngle(
            axis,
            new_branch.axis,
            rot_axis);
    std::cout << std::endl;
    std::cout << rot_axis.x << ' ' << rot_axis.y << ' ' << rot_axis.z <<  ' ' << angle << std::endl;
    new_branch.rotate(rot_axis, -angle);
    *this = new_branch;
}

void Branch::rotate(glm::vec3 ax, float val) {
    for (int i = 0; i < points.size(); i++) {
        points[i] = center + glm::rotate(points[i] - center, val, ax);
    }
    axis = glm::rotate(axis, val, ax);
}

glm::vec3 Branch::get_shift() const {
    return axis * height;
}

float Branch::get_bottom_r() const {
    return bot_radius;
}

std::vector<float> Branch::to_normals() const {
    std::vector<float> result;

    for (int i = 0; i < normals.size(); i++) {
        std::vector<int> round;
        if (i % 2 == 0) {
            round = {0, 2, 1};
        } else {
            round = {0, 1, 2};
        }

        for (auto j : round) {
            int index = (j + i) % normals.size();
            result.push_back(normals[index].x);
            result.push_back(normals[index].y);
            result.push_back(normals[index].z);
            //result.push_back(1.0f);
        }
    }

    return result;
}

std::vector<float> Branch::to_points() const {
    std::vector<float> result;

    for (int i = 0; i < points.size(); i++) {
        std::vector<int> round;
        if (i % 2 == 0) {
            round = {0, 2, 1};
        } else {
            round = {0, 1, 2};
        }

        for (auto j : round) {
            int index = (j + i) % points.size();
            result.push_back(points[index].x);
            result.push_back(points[index].y);
            result.push_back(points[index].z);
        }
    }

    return result;
}

int Branch::size() const {
    return points.size();
}

glm::vec3 Branch::next(const glm::vec3 &input) const {
    return glm::rotate(input, 2 * PI / fragmentation, glm::vec3(0, 0, 1));
}

void Branch::build() {
    float alpha = 2 * PI / fragmentation;

    glm::vec3 init_bot(bot_radius, 0, 0);
    glm::vec3 init_top(top_radius, 0, height);
    init_top = glm::rotate(init_top, alpha / 2, glm::vec3(0, 0, 1));

    glm::vec3 init_bot_normal(height / l, 0, (bot_radius - top_radius) / l);
    glm::vec3 init_top_normal = glm::rotate(init_bot_normal, alpha / 2, glm::vec3(0, 0, 1));

    points.push_back(init_bot);
    points.push_back(init_top);
    normals.push_back(init_bot_normal);
    normals.push_back(init_top_normal);

    for (int i = 0; i < fragmentation - 1; i++) {
        auto bot = points[points.size() - 2];
        auto top = points[points.size() - 1];
        auto bot_normal = normals[normals.size() - 2];
        auto top_normal = normals[normals.size() - 1];

        points.push_back(next(bot));
        points.push_back(next(top));
        normals.push_back(next(bot_normal));
        normals.push_back(next(top_normal));
    }
}

